from pymongo import MongoClient
from werkzeug.exceptions import BadRequest
from flask import jsonify
import datetime


client = MongoClient('mongodb://localhost:27017')

db = client['kenzie']


class Post():
    posts_list = list(db.posts.find())
    id = 1 if len(posts_list) == 0 else posts_list[-1]['id'] + 1

    def __init__(self, title: str, author: str, tags: list, content: str):
        self.id = self.id
        self.created_at = datetime.datetime.utcnow()
        self.updated_at = ''
        self.title = title
        self.author = author
        self.tags = tags
        self.content = content

    def create_post(self):
        self.generate_id()
        db.posts.insert_one(self.__dict__)

    @classmethod
    def generate_id(cls):
        cls.id = cls.id + 1

    @staticmethod
    def delete_specific_post(post_id):
        post = db.posts.find_one_and_delete({"id": int(post_id)})
        if post != None:
            del post["_id"]
            return jsonify(post), 200
        else:
            return {"message": "Post não encontado"}, 404

    @staticmethod
    def get_all():
        posts_list = list(db.posts.find())

        for post in posts_list:
            del post['_id']

        return posts_list

    @staticmethod
    def get_specific(post_id):
        try:
            post = db.posts.find_one({"id": int(post_id)})
            del post['_id']
            return post
        except:
            return {"message": "Post não encontrado"}, 404

    @staticmethod
    def update_post_data(post_id, update_value):

        update_value['updated_at'] = datetime.datetime.utcnow()
        data = {"$set": update_value}
        post = db.posts.find_one({"id": int(post_id)})
        if post:
            try:
                db.posts.update_one(post, data)

                updated_post = db.posts.find_one({"id": int(post_id)})
                del updated_post['_id']
                return updated_post, 202
            except BadRequest:
                return {"message": "Entrada inválida"}, 400
        else:
            return {"message": "Post não encontrado"}, 404

    @staticmethod
    def get_post_tags(id):
            post = db.posts.find_one({'id': id})
            post_tags = post['tags']
            return jsonify(post_tags), 200


    def __repr__(self):
        output = {
            "id": self.id,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "title": self.title,
            "author": self.author,
            "tags": f'http://localhost:5000/posts/{self.id}/tags',
            "contet": self.content,
        }

        return output
