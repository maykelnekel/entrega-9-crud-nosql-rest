from flask import jsonify, Flask, request
from ujson import load
from app.exceptions.posts_exceptions import InvalidPostError, InvalidPostIdError
from app.models.ticket_model import Post
import pdb

def init_app(app: Flask):
    @app.get('/posts')
    def get_all_posts():
        posts_list = Post.get_all()

        return jsonify(posts_list)

    @app.get('/posts/<int:post_id>')
    def get_specific_post(post_id):
        post = Post.get_specific(post_id)

        try:
            return jsonify(post), 200
        except (InvalidPostIdError, IndexError):
            return {"message": "post não encontrado"}, 404

    @app.get('/posts/<int:post_id>/tags')
    def get_tag(post_id):
        try:
            return Post.get_post_tags(post_id)
        except TypeError:
            return {"message": "id não enncontrado"}, 404

    @app.post('/posts')
    def create_post():
        data = request.json

        try:
            new_post = Post(**data)
            new_post.create_post()
            created_new_post = new_post.__repr__()

            return jsonify(created_new_post), 201

        except (InvalidPostError, TypeError, SyntaxError, EOFError):
            return {"message": "Dados inválidos"}, 400

    @app.delete('/posts/<int:post_id>')
    def delet_post(post_id):
        return Post.delete_specific_post(post_id)

    @app.patch('/posts/<int:post_id>')
    def update_post(post_id):
        data = request.json

        return Post.update_post_data(post_id, data)

